package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"syscall"
	"time"

	aiplatform "cloud.google.com/go/aiplatform/apiv1"
	"google.golang.org/api/option"

	"gitlab.com/picnic-app/backend/generative-api/internal/controller"
	"gitlab.com/picnic-app/backend/generative-api/internal/service"
	grpc "gitlab.com/picnic-app/backend/generative-api/internal/util/grpcclient"
	utils "gitlab.com/picnic-app/backend/generative-api/internal/util/url"
	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/core"
	"gitlab.com/picnic-app/backend/libs/golang/core/mw"
	"gitlab.com/picnic-app/backend/libs/golang/graceful"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/monitoring"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
	grpcStorage "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/storage-api/storage/v1"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	app, err := initApp(ctx)
	if err != nil {
		log.Fatal(err)
	}
	cfg := app.Config()

	err = tracing.SetupExporter(ctx)
	if err != nil {
		logger.Errorf(ctx, "failed to set up tracing exporter: %v", err)
	}

	monitoring.RegisterPrometheusSuffix()

	debugSrv, err := app.RunDebug(ctx)
	if err != nil {
		logger.Fatalf(ctx, "failed to start debug server: %v", err)
	}

	ctrl, cleanup := initController(ctx, cfg)
	defer cleanup()

	gracefulShutdown := graceful.New(
		&graceful.ShutdownManagerOptions{Timeout: 60 * time.Second},
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "servers",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				app.Close()
				return nil
			}),
			graceful.HTTPServer(debugSrv),
		),
		graceful.Context(cancel),
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "clients",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				cleanup()
				return nil
			}),
			graceful.Tracer(),
		),
		graceful.Logger(nil),
	)
	gracefulShutdown.RegisterSignals(os.Interrupt, syscall.SIGTERM)
	defer func() {
		_ = gracefulShutdown.Shutdown(context.Background())
	}()

	err = app.Run(ctx, ctrl)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	logger.Info(ctx, "gRPC server closed gracefully")
}

func initApp(ctx context.Context) (core.Application, error) {
	app, err := core.InitAppWithOptions(ctx, mw.LogOptions{
		RedactRequestFunc:  func(a any) any { return a },
		RedactResponseFunc: func(a any) any { return a },
	})
	if err != nil {
		return nil, err
	}

	app = app.WithMW(mw.NewServerContextInterceptor(config.String("env.auth.secret")))

	return app, nil
}

func initController(ctx context.Context, cfg config.Config) (controller.Controller, func()) {
	utils.InitURLCompleter(config.String("env.svc.storage.url"))

	storageClient, err := grpc.NewClient(ctx,
		config.String("env.svc.storage.host"),
		config.Int("env.svc.storage.port"),
		config.Bool("env.svc.storage.secure"),
		cfg.ServiceName,
		config.String("env.auth.secret"),
	)
	if err != nil {
		logger.Fatalf(ctx, "Failed to create client to storage-api: %v", err)
	}
	storageCli := grpcStorage.NewStorageAPIClient(storageClient)

	predictionLocation := config.String("env.predictions.location")
	endpoint := fmt.Sprintf("%s-aiplatform.googleapis.com:443", predictionLocation)

	predictionClient, err := aiplatform.NewPredictionClient(
		ctx,
		option.WithEndpoint(endpoint),
		option.WithCredentialsFile(config.String("env.google_adc")),
	)
	if err != nil {
		logger.Fatalf(ctx, "Failed to create prediction client: %v", err)
	}

	predictionConfig, err := service.NewPredictionConfig(
		config.String("env.predictions.project_id"),
		config.String("env.predictions.endpoint_id"),
		predictionLocation,
	)
	if err != nil {
		logger.Fatalf(ctx, "Failed to create prediction config: %v", err)
	}

	svc := service.New(
		storageCli,
		predictionClient,
		predictionConfig,
	)

	return controller.New(svc), func() {
		_ = storageClient.Close()
		_ = predictionClient.Close()
	}
}
