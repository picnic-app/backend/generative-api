#!/bin/sh

# this is a migration script to be used in docker container as a migration entrypoint
# it is NOT intended for local use

# example command to run migration script within docker container (assuming workload identity is in order):
# docker run container-name /bin/sh /db/migrate.sh

/app/linkerd-await --shutdown -- /app/wrench migrate up --directory /app \
  --project "${SPANNER_PROJECT}" \
  --instance "${SPANNER_INSTANCE}" \
  --database "${SPANNER_DATABASE}"
