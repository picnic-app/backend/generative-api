package convert

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestToPointers(t *testing.T) {
	array := []int{2, 3}

	got := ToPointers(array)
	assert.Equal(t, 2, *got[0])
	assert.Equal(t, 3, *got[1])
}

func TestToValues(t *testing.T) {
	num1 := 2
	num2 := 3
	array := []*int{&num1, &num2}

	got := ToValues(array)
	assert.Equal(t, 2, got[0])
	assert.Equal(t, 3, got[1])
}
