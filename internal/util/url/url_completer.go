package url

import (
	"fmt"
	"strings"
)

// URLCompleter is a struct that is initialisied in main and it posses the publicURL for our storage
type URLCompleter struct {
	publicURL        string
	shareLinkHostURL string
}

// URLHelper is a singleton of URLCompleter that offers some functions
var URLHelper URLCompleter

// InitURLCompleter is called from main to inject baseURL for our public storage.
// This function is called in main before any graphql related stuff
func InitURLCompleter(baseURL string) {
	URLHelper = URLCompleter{
		publicURL: baseURL,
		// TODO: Make this dynamic
		shareLinkHostURL: "https://picnic.zone",
	}
}

// GetOptionalPublicURL ..
func (c *URLCompleter) GetOptionalPublicURL(in *string) *string {
	if in == nil || *in == "" {
		return nil
	}

	ret := c.GetPublicURL(*in)

	return &ret
}

// GetPublicURL takes the filename that is recorded on entity and returns the URL of the file
// Exaple input and output ;
// profile/avatar.jpg ---> https://media.picnic.com/profile/avatar.jpg
func (c *URLCompleter) GetPublicURL(fileName string) string {
	if strings.HasPrefix(fileName, "http") {
		return fileName
	}

	return fmt.Sprintf("%s/%s", c.publicURL, fileName)
}

// GetPublicURLForThumb returns Thumbnail version version of the given image/video filename.
// Exaple input and output ;
// profile/avatar.jpg ---> https://media.picnic.com/profile/avatar_thumb.jpg
func (c *URLCompleter) GetPublicURLForThumb(fileName string) string {
	if strings.HasPrefix(fileName, "http") {
		return fileName
	}

	thumbName := strings.Replace(fileName, ".", "_thumb.", 1)
	return c.GetPublicURL(thumbName)
}
