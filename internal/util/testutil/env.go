package testutil

import (
	"os"
	"strings"
)

func IsDevEnv() bool {
	return strings.ToLower(os.Getenv("DEV")) == "true"
}
