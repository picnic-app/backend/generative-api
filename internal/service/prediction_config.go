package service

import "fmt"

type PredictionConfig struct {
	projectID  string
	endpointID string
	location   string
}

func (p PredictionConfig) Location() string {
	return p.location
}

func (p PredictionConfig) EndpointID() string {
	return p.endpointID
}

func (p PredictionConfig) ProjectID() string {
	return p.projectID
}

func NewPredictionConfig(
	projectID string,
	endpointID string,
	location string,
) (PredictionConfig, error) {
	if projectID == "" {
		return PredictionConfig{}, fmt.Errorf("projectID is empty")
	}

	if endpointID == "" {
		return PredictionConfig{}, fmt.Errorf("endpointID is empty")
	}

	if location == "" {
		return PredictionConfig{}, fmt.Errorf("location is empty")
	}

	return PredictionConfig{projectID: projectID, endpointID: endpointID, location: location}, nil
}
