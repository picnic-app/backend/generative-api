package service

import (
	"context"

	"cloud.google.com/go/aiplatform/apiv1/aiplatformpb"
	"github.com/googleapis/gax-go/v2"

	storagePb "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/storage-api/storage/v1"
)

type PredictionClient interface {
	Predict(
		ctx context.Context,
		req *aiplatformpb.PredictRequest,
		options ...gax.CallOption,
	) (*aiplatformpb.PredictResponse, error)
}

func New(
	storageClient storagePb.StorageAPIClient,
	predictionClient PredictionClient,
	predictionConfig PredictionConfig,
) Service {
	return Service{
		GenerateImageHandler: NewGenerateImageHandler(
			storageClient,
			predictionClient,
			predictionConfig,
		),
	}
}

type Service struct {
	GenerateImageHandler
}
