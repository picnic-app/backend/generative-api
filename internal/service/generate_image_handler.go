package service

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"io"

	"cloud.google.com/go/aiplatform/apiv1/aiplatformpb"
	"google.golang.org/protobuf/types/known/structpb"

	"gitlab.com/picnic-app/backend/generative-api/internal/util/url"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	grpcStorage "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/storage-api/storage/v1"
)

type GenerateImageHandler struct {
	storageClient    grpcStorage.StorageAPIClient
	predictionClient PredictionClient
	config           PredictionConfig
}

func NewGenerateImageHandler(
	storageClient grpcStorage.StorageAPIClient,
	predictionClient PredictionClient,
	config PredictionConfig,
) GenerateImageHandler {
	return GenerateImageHandler{
		storageClient:    storageClient,
		predictionClient: predictionClient,
		config:           config,
	}
}

func (h GenerateImageHandler) GenerateImage(
	ctx context.Context,
	style,
	prompt string,
) (string, error) {
	parameters, err := structpb.NewValue(map[string]interface{}{})
	if err != nil {
		return "", err
	}

	prompt = createPrompt(prompt, style)

	instance, err := structpb.NewValue(map[string]interface{}{
		"prompt": prompt,
	})
	if err != nil {
		return "", err
	}

	endpoint := fmt.Sprintf("projects/%s/locations/%s/endpoints/%s",
		h.config.ProjectID(),
		h.config.Location(),
		h.config.EndpointID(),
	)
	req := &aiplatformpb.PredictRequest{
		Endpoint:   endpoint,
		Instances:  []*structpb.Value{instance},
		Parameters: parameters,
	}

	resp, err := h.predictionClient.Predict(ctx, req)
	if err != nil {
		return "", err
	}

	if len(resp.Predictions) == 0 {
		return "", fmt.Errorf("no predictions returned")
	}

	imageBase64 := resp.Predictions[0].GetStringValue()
	logger.Infof(ctx, "Predictions: '%s'", resp.Predictions[0].GetStringValue())

	decodedImage, err := base64.StdEncoding.DecodeString(imageBase64)
	if err != nil {
		logger.Errorf(ctx, "Error decoding image: %v", err)
		return "", err
	}

	reader := bytes.NewReader(decodedImage)
	rsp, err := h.uploadFile(ctx, "image/jpeg", reader.Size(), "generative", reader)
	if err != nil {
		logger.Errorf(ctx, "Error uploading image: %v", err)
		return "", err
	}

	logger.Debugf(ctx, "Upload info: '%s'", rsp.GetInfo().String())

	imageUrl := url.URLHelper.GetPublicURL(rsp.GetInfo().GetFilename())

	return imageUrl, nil
}

func (h GenerateImageHandler) uploadFile(
	ctx context.Context,
	contentType string,
	fileSize int64,
	service string,
	reader io.Reader,
) (*grpcStorage.UploadFileResponse, error) {
	uploadStream, err := h.storageClient.UploadFile(ctx)
	if err != nil {
		return nil, err
	}

	err = uploadStream.SendMsg(&grpcStorage.UploadFileRequest{
		Messages: &grpcStorage.UploadFileRequest_Info{
			Info: &grpcStorage.FileInfo{
				Type:    contentType,
				Size:    fileSize,
				Service: service,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	sendChunk := func(buf []byte) error {
		return uploadStream.Send(&grpcStorage.UploadFileRequest{
			Messages: &grpcStorage.UploadFileRequest_Data{Data: buf},
		})
	}

	buf := make([]byte, 1024)
	for {
		if _, err = reader.Read(buf); err != nil {
			if err != io.EOF {
				return nil, err
			}

			if err = sendChunk(buf); err != nil {
				return nil, err
			}
			break
		}

		if err = sendChunk(buf); err != nil {
			return nil, err
		}
	}

	return uploadStream.CloseAndRecv()
}

func createPrompt(prompt string, style string) string {
	if style == "" {
		return prompt
	}

	return fmt.Sprintf("%s in style %s", prompt, style)
}
