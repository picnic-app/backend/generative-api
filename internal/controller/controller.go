package controller

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"google.golang.org/grpc"

	"gitlab.com/picnic-app/backend/generative-api/internal/service"
	generativeV1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/generative-api/generative/v1"
)

func New(svc service.Service) Controller {
	return Controller{
		service: svc,
	}
}

// Controller represents struct that implements ProfileServiceServer.
type Controller struct {
	service service.Service

	generativeV1.UnsafeGenerativeApiServiceServer
}

func (c Controller) Register(reg grpc.ServiceRegistrar) {
	generativeV1.RegisterGenerativeApiServiceServer(reg, c)
}

func (c Controller) GenerateImage(
	ctx context.Context,
	request *generativeV1.GenerateImageRequest,
) (*generativeV1.GenerateImageResponse, error) {
	if request.GetPrompt() == "" {
		return nil, status.Error(codes.InvalidArgument, "prompt is required")
	}

	imageUrl, err := c.service.GenerateImage(ctx, request.Style, request.Prompt)
	if err != nil {
		return nil, err
	}

	return &generativeV1.GenerateImageResponse{
		ImageUrl: imageUrl,
	}, nil
}
