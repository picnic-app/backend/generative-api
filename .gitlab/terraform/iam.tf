module "workload-identity" {
  source  = "gitlab.com/picnic-app/workload-identity/google"
  version = "~> 1.0"

  service_name = var.service_name
  create_sa    = local.is_not_dev
  sa_name      = var.service_name
  namespace    = var.env_id
}

resource "google_project_iam_member" "vertex_endpoint_prediction" {
  count = (local.is_not_dev && !local.is_perf) ? 1 : 0

  member  = "serviceAccount:${module.workload-identity.service_account.email}"
  project = local.project
  role    = "organizations/${data.google_organization.this.org_id}/roles/aiplatform.predictionViewer"
}