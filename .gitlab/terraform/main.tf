locals {
  project    = module.project.project
  is_not_dev = contains(["stg", "prod"], var.env_id)
  is_perf    = var.env_id == "perf"
}

module "project" {
  source  = "gitlab.com/picnic-app/project/google"
  version = "~> 1.0"

  env_id = var.env_id
}

data "google_organization" "this" {
  domain = "picnic.zone"
}